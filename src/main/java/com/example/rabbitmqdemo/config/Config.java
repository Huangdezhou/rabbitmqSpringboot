package com.example.rabbitmqdemo.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {
    public static final String SMS_QUEUE = "SMS_QUEUE";
    public static final String EMAIL_QUEUE = "EMAIL_QUEUE";
    public static final String exchage_naem = "fanout";

    @Bean
    public FanoutExchange fanoutExchange() {
        return new FanoutExchange(exchage_naem, true, false);
    }

    @Bean
    public Queue smsqueue() {
        return new Queue(SMS_QUEUE, true,
                false, false, null);
    }

    @Bean
    public Queue emailqueue() {
        return new Queue(EMAIL_QUEUE, true,
                false, false, null);
    }

    @Bean
    public Binding smsBingding(Queue smsqueue, Exchange fanoutExchange) {
        return BindingBuilder.bind(smsqueue).to(fanoutExchange).with("").noargs();
    }

    @Bean
    public Binding emailBingding(Queue emailqueue, Exchange fanoutExchange) {
        return BindingBuilder.bind(emailqueue).to(fanoutExchange).with("").noargs();
    }

}
