package com.example.rabbitmqdemo.consumer;

import com.example.rabbitmqdemo.entity.MsgEntity;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import java.io.IOException;


@Service
@Slf4j
public class Comsumer {

    @RabbitListener(queues = "SMS_QUEUE")
    public void list(MsgEntity msgEntity, Message md
            , Channel channel) throws IOException {
        System.out.println("SMS_QUEUE:+" + msgEntity);
        channel.basicAck(md.getMessageProperties().getDeliveryTag(), false);
    }

    @RabbitListener(queues = "EMAIL_QUEUE")
    public void EMAIL_QUEUE(MsgEntity msgEntity, Message md
            , Channel channel)throws  Exception {
        channel.basicAck(md.getMessageProperties().getDeliveryTag(), false);
        System.out.println("EMAIL_QUEUE:+" + msgEntity);
    }
}
