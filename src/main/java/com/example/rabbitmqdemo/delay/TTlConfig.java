//package com.example.rabbitmqdemo.delay;
//
//import org.springframework.amqp.core.*;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import java.util.HashMap;
//import java.util.Map;
//
//@Configuration
//public class TTlConfig {
//
//
//    //普通队列
//    public static final String delay_queue = "delay_queue";
//    public static final String delay_exchange = "delay_exchange";
//    public static final String delay_routing_key = "delay_routing_key";
//
//    @Bean(delay_exchange)
//    public CustomExchange customExchange() {
//        Map<String, Object> pros = new HashMap<>();
//        pros.put("x-delayed-type", "direct");
//        return new CustomExchange(delay_exchange, "x-delayed-message", true, false, pros);
//    }
//
//
//    @Bean(delay_queue)
//    public Queue queueDelay() {
//        return QueueBuilder.durable(delay_queue).build();
//    }
//
//
//    @Bean
//    public Binding queueABingdingX(@Qualifier(delay_queue) Queue queueA,
//                                   @Qualifier(delay_exchange) CustomExchange exchange) {
//        return BindingBuilder.bind(queueA).to(exchange).with(delay_routing_key).noargs();
//    }
//
//
//}
