package com.example.rabbitmqdemo.entity;

import lombok.Data;

import java.io.Serializable;
@Data
public class MsgEntity  implements Serializable {
    private  String msgId;
    private  String userId;
    private  String phone;
    private  String email;

    public MsgEntity(String msgId, String userId, String phone, String email) {
        this.msgId = msgId;
        this.userId = userId;
        this.phone = phone;
        this.email = email;
    }
}
