//package com.example.rabbitmqdemo.firve;
//
//import org.springframework.amqp.core.Binding;
//import org.springframework.amqp.core.BindingBuilder;
//import org.springframework.amqp.core.DirectExchange;
//import org.springframework.amqp.core.Queue;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import java.util.HashMap;
//import java.util.Map;
//
//@Configuration
//public class Config {
//    @Bean
//    public DirectExchange DirectExchange() {
//        return new DirectExchange("order_exchange", true, false);
//    }
//
//    @Bean
//    public Queue ttlQueue() {
//        return new Queue("orderqueue.Queue4", true,
//                false, false, null);
//    }
//
//    @Bean
//    public Binding smsBingding() {
//        return BindingBuilder.bind(ttlQueue()).to(DirectExchange()).with("sms");
//    }
//
//}
