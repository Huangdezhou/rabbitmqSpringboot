//package com.example.rabbitmqdemo.four;
//
//import org.springframework.amqp.core.Binding;
//import org.springframework.amqp.core.BindingBuilder;
//import org.springframework.amqp.core.DirectExchange;
//import org.springframework.amqp.core.Queue;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import java.util.HashMap;
//import java.util.Map;
//
//@Configuration
//public class Config {
//    @Bean
//    public DirectExchange DirectExchange() {
//        return new DirectExchange("ttl_order_exchange3", true, false);
//    }
//
//    @Bean
//    public Queue ttlQueue() {
//        Map<String, Object> prop = new HashMap<>();
////        prop.put("x-message-ttl", 6000);
////        prop.put("x-dead-letter-exchange", "dead_order_exchange4");
////        prop.put("x-max-length", 15);
////        prop.put("x-dead-letter-routing-key", "dead");
//        return new Queue("ttl.Direct.Queue4", true,
//                false, false, prop);
//    }
//
//    @Bean
//    public Binding smsBingding() {
//        return BindingBuilder.bind(ttlQueue()).to(DirectExchange()).with("sms");
//    }
//
//}
