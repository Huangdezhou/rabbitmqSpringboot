//package com.example.rabbitmqdemo.four;
//
//import org.springframework.amqp.core.Binding;
//import org.springframework.amqp.core.BindingBuilder;
//import org.springframework.amqp.core.DirectExchange;
//import org.springframework.amqp.core.Queue;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import java.util.HashMap;
//import java.util.Map;
//
//@Configuration
//public class DeadConfig {
//    @Bean
//    public DirectExchange deadExchange() {
//        return new DirectExchange("dead_order_exchange4", true, false);
//    }
//
//    @Bean
//    public Queue dead2Queue() {
//        return new Queue("dead.Queue3", true,
//                false, false, null);
//    }
//
//    @Bean
//    public Binding deadBingding() {
//        return BindingBuilder.bind(dead2Queue()).to(deadExchange()).with("dead");
//    }
//
//}
