package com.example.rabbitmqdemo.producer;

import com.example.rabbitmqdemo.entity.MsgEntity;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
public class ProducerService {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @RequestMapping("/")
    public String sendmsg() throws InterruptedException {
        MsgEntity msgEntity = new MsgEntity(UUID.randomUUID().toString(), "ssss", "2222", "ss@qq.com");
        rabbitTemplate.convertAndSend("fanout", "", msgEntity);
        return "b";
    }
}
