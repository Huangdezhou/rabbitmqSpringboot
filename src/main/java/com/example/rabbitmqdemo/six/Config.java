//package com.example.rabbitmqdemo.six;
//
//import org.springframework.amqp.core.*;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//@Configuration
//public class Config {
//    public static final String EXHANGE_NAME = "BOOT_EXHCANGE_TOPIC2";
//    public static final String QUEUE_NAME = "QUEUE_NAME2";
//@Bean
//    public Exchange exchange() {
//        return new DirectExchange(EXHANGE_NAME);
//    }
//
//    @Bean
//    public Queue bootqueue() {
//        return new Queue(QUEUE_NAME, true);
//    }
//
//    @Bean
//    public Binding binding(Queue bootqueue, Exchange exchange) {
//        return BindingBuilder.bind(bootqueue).to(exchange).with("test").noargs();
//    }
//}
