package com.example.rabbitmqdemo.six;//package com.example.rabbitmqdemo.four;

import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class OrderService {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void mackOrder() {
        String exchange = "BOOT_EXHCANGE_TOPIC2";
        String orderId = UUID.randomUUID().toString();
        for (int i = 0; i < 100000; i++) {
         rabbitTemplate.convertAndSend(exchange, "test", orderId);
        }
    }
}
