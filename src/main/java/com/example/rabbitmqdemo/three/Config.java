//package com.example.rabbitmqdemo.three;
//
//import org.springframework.amqp.core.Binding;
//import org.springframework.amqp.core.BindingBuilder;
//import org.springframework.amqp.core.DirectExchange;
//import org.springframework.amqp.core.Queue;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//@Configuration
//public class Config {
//    @Bean
//    public DirectExchange DirectExchange() {
//        return new DirectExchange("Direct_order_exchange", true, false);
//    }
//
//    @Bean
//    public Queue smsQueue() {
//        return new Queue("sms.Direct.Queue", true, false, false, null);
//    }
//
//    @Bean
//    public Queue emailQueue() {
//        return new Queue("mail.Direct.Queue", true, false, false, null);
//    }
//
//    @Bean
//    public Queue wechatQueue() {
//        return new Queue("wechat.Direct.Queue", true, false, false, null);
//    }
//
//    @Bean
//    public Binding smsBingding() {
//        return BindingBuilder.bind(smsQueue()).to(DirectExchange()).with("sms");
//    }
//
//    @Bean
//    public Binding emailBingding() {
//        return BindingBuilder.bind(emailQueue()).to(DirectExchange()).with("email");
//    }
//
//    @Bean
//    public Binding wechatBingding() {
//        return BindingBuilder.bind(wechatQueue()).to(DirectExchange()).with("wechat");
//    }
//}
