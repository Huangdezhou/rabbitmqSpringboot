//package com.example.rabbitmqdemo.two.service;
//
//import org.springframework.amqp.core.Binding;
//import org.springframework.amqp.core.BindingBuilder;
//import org.springframework.amqp.core.FanoutExchange;
//import org.springframework.amqp.core.Queue;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//@Configuration
//public class Config {
//    @Bean
//    public FanoutExchange fanoutExchange() {
//        return new FanoutExchange("fanout_order_exchange", true, false);
//    }
//
//    @Bean
//    public Queue smsQueue() {
//        return new Queue("sms.fanout.Queue", true, false, false, null);
//    }
//
//    @Bean
//    public Queue emailQueue() {
//        return new Queue("mail.fanout.Queue", true, false, false, null);
//    }
//
//    @Bean
//    public Queue wechatQueue() {
//        return new Queue("wechat.fanout.Queue", true, false, false, null);
//    }
//
//    @Bean
//    public Binding smsBingding() {
//        return BindingBuilder.bind(smsQueue()).to(fanoutExchange());
//    }
//
//    @Bean
//    public Binding emailBingding() {
//        return BindingBuilder.bind(emailQueue()).to(fanoutExchange());
//    }
//
//    @Bean
//    public Binding wechatBingding() {
//        return BindingBuilder.bind(wechatQueue()).to(fanoutExchange());
//    }
//}
