//package com.example.rabbitmqdemo.two.service;
//
//import org.springframework.amqp.rabbit.core.RabbitTemplate;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.util.UUID;
//
//@Service
//
//public class OrderService {
//    @Autowired
//    private RabbitTemplate rabbitTemplate;
//
//    public void mackOrder(String orderid,String productId,Integer num){
//        String exchange="fanout_order_exchange";
//        String orderId= UUID.randomUUID().toString();
//        rabbitTemplate.convertAndSend(exchange,"",orderId);
//    }
//}
